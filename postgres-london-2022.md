[comment]: # (This presentation was made with markdown-slides)
[comment]: # (This is a CommonMark compliant comment. It will not be included in the presentation.)
[comment]: # (Compile this presentation with the command below)
[comment]: # (mdslides presentation.md --include media)

[comment]: # (Set the theme:)
[comment]: # (THEME = white)
[comment]: # (CODE_THEME = base16/zenburn)
[comment]: # (The list of themes is at https://revealjs.com/themes/)
[comment]: # (The list of code themes is at https://highlightjs.org/)

[comment]: # "You can also use quotes instead of parenthesis"
[comment]: # "example https://github.com/dadoomer/markdown-slides/blob/master/example/presentation.md"
[comment]: # "THEME = white"

[comment]: # (Pass optional settings to reveal.js:)
[comment]: # (controls: true)
[comment]: # (keyboard: true)
[comment]: # (markdown: { smartypants: true })
[comment]: # (hash: false)
[comment]: # (respondToHashChanges: false)
[comment]: # (Other settings are documented at https://revealjs.com/config/)

### backend.sql ♥ frontend.js

@nileshtrivedi

[comment]: # (A comment starting with three or more !!! marks a slide break.)

[comment]: # (!!!)

- This is an idea for open-source, self-hosted webapps
  - Apps for things like todos, notes, tools
  - multi-user, multi-device
- The overarching goal is simplicity of the stack

[comment]: # (!!! data-auto-animate)

- This is an idea for open-source, self-hosted webapps
- The overarching goal is simplicity of the stack
  - For users: Fewer components to install, secure, maintain/upgrade

[comment]: # (!!! data-auto-animate)

- This is an idea for open-source, self-hosted webapps
- The overarching goal is simplicity of the stack
  - For users: Fewer components to install, secure, maintain/upgrade
  - For developers: 

[comment]: # (!!! data-auto-animate)

- This is an idea for open-source, self-hosted webapps
- The overarching goal is simplicity of the stack
  - For users: Fewer components to install, secure, maintain/upgrade
  - For developers: 
    - Fewer languages (more devs)

[comment]: # (!!! data-auto-animate)

- This is an idea for open-source, self-hosted webapps
- The overarching goal is simplicity of the stack
  - For users: Fewer components to install, secure, maintain/upgrade
  - For developers: 
    - Fewer languages (more devs)
    - Less code to write (fewer bugs)

[comment]: # (!!! data-auto-animate)

- This is an idea for open-source, self-hosted webapps
- The overarching goal is simplicity of the stack
  - For users: Fewer components to install, secure, maintain/upgrade
  - For developers: 
    - Fewer languages (more devs)
    - Less code to write (fewer bugs)
    - Easier maintenance (happier life)

[comment]: # (!!! data-auto-animate)

#### PostgreSQL is awesome!

[comment]: # (!!! data-auto-animate)

#### PostgreSQL is awesome!

- Rich data types (JSON, PostGIS and more)

[comment]: # (!!! data-auto-animate)

#### PostgreSQL is awesome!
    
- Rich data types (JSON, PostGIS and more)
- Row-level security (i.e. role-based access)

[comment]: # (!!! data-auto-animate)

#### PostgreSQL is awesome!
    
- Rich data types (JSON, PostGIS and more)
- Row-level security (i.e. role-based access)
- Materialized and non-materialized views

[comment]: # (!!! data-auto-animate)

#### PostgreSQL is awesome!
    
- Rich data types (JSON, PostGIS and more)
- Row-level security (i.e. role-based access)
- Materialized and non-materialized views
- Foreign data wrappers

[comment]: # (!!! data-auto-animate)

#### PostgreSQL is awesome!
    
- Rich data types (JSON, PostGIS and more)
- Row-level security (i.e. role-based access)
- Materialized and non-materialized views
- Foreign data wrappers
- Stored procedures (even in Javascript or Python)

[comment]: # (!!! data-auto-animate)

#### PostgreSQL is awesome!
    
- Rich data types (JSON, PostGIS and more)
- Row-level security (i.e. role-based access)
- Materialized and non-materialized views
- Foreign data wrappers
- Stored procedures (even in Javascript or Python)
- Extensions, LISTEN/NOTIFY

[comment]: # (!!! data-auto-animate)

So, PostgreSQL has everything that you need in a backend.

But it does not speak HTTP.

[comment]: # (!!!)

And it's kind of silly to bring in a whole language run time and developer skills just to do this basic SQL-to-JSON plumbing.

[comment]: # (!!!)

![postgrest](media/postgrest.png)

[comment]: # (!!!)

#### PostgREST is awesome!

[comment]: # (!!! data-auto-animate)

#### PostgREST is awesome!

- Generates a high-performance, and secure REST api

[comment]: # (!!! data-auto-animate)

#### PostgREST is awesome!

- Generates a high-performance, and secure REST api
- Can use JWT to switch into roles with row-level security

[comment]: # (!!! data-auto-animate)

#### PostgREST is awesome!

- Generates a high-performance, and secure REST api
- Can use JWT to switch into roles with row-level security
- Can expose stored procedures to HTTP

[comment]: # (!!! data-auto-animate)

#### PostgREST is awesome!

- Generates a high-performance, and secure REST api
- Can use JWT to switch into roles with row-level security
- Can expose stored procedures to HTTP
- Allows custom HTTP response headers

[comment]: # (!!! data-auto-animate)

```
GET /people?select=first_name,age,json_data->phones
    &or=(age.lt.18,age.gt.21) HTTP/1.1

GET /tsearch?my_tsv=fts(french).amusant HTTP/1.1

POST /rpc/function_name HTTP/1.1
```

```
PUT /employees?id=eq.4 HTTP/1.1

{ "id": 4, "name": "Sara B.", "salary": 60000 }
```


[comment]: # (!!!)

So, PostgreSQL + PostgREST has everything we need in the backend.

What about the frontend?

[comment]: # (!!!)

![setting custom response header](media/postgrest2.png)

[comment]: # (!!!)

![idea](media/rpchomepage.png)

[comment]: # (!!!)

![idea](media/arch1.png)

[comment]: # (!!!)

![idea](media/arch2.png)

[comment]: # (!!!)

# [Show me the code!](https://github.com/PolyglotNetwork/postodo/)

[comment]: # (!!!)

#### This has two big benefits:

- There is no backend code to be written anywhere. The database schema and custom functions provide all the server-side functions an app needs. Entire programming language gets eliminated from our stack.

[comment]: # (!!! data-auto-animate)

#### This has two big benefits:

- There is no backend code to be written anywhere. The database schema and custom functions provide all the server-side functions an app needs. Entire programming language gets eliminated from our stack.
- The frontend can be loaded from CDN and the user doesn't have to update it whenever developers ship bugfixes or improvements.

[comment]: # (!!! data-auto-animate)

#### This has two big benefits:

- There is no backend code to be written anywhere. The database schema and custom functions provide all the server-side functions an app needs. Entire programming language gets eliminated from our stack.
- The frontend can be loaded from CDN and the user doesn't have to update it whenever developers ship bugfixes or improvements.
- Especially convenient for self-hosted open-source webapps

[comment]: # (!!! data-auto-animate)

## What about non-CRUD operations?

- Background / Periodic jobs: pg_background, pg_cron
- External data: Foreign data wrappers, pg_http
- Machine learning: Apache MADlib
- Webhooks: /rpc, pgpodman
- Static files: Supabase storage

[comment]: # (!!!)

![idea](media/supabase.png)

[comment]: # (!!!)


![idea](media/solid.png)

[comment]: # (!!!)

![idea](media/datastore.png)

[comment]: # (!!!)

## Thank you

For questions/critique/ideas/discussions: 

[github.com/nileshtrivedi/sqljs](https://github.com/nileshtrivedi/sqljs)

