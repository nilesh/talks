# Talks

This repo contains the source for all my past and future presentations.

I prefer to use [markdown-slides](https://github.com/dadoomer/markdown-slides) to transform my 
markdown notes into presentations for easier version-control.

Generate slides with: `mdslides ./presentation.md --include media`

You can view slides here:

- [Postgres London 2022: backend.sql ♥ frontend.js](https://nilesh.codeberg.page/talks/postgres-london-2022/)
